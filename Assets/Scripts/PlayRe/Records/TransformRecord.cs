using System.Runtime.Serialization;
using PlayRe.Internal;
using PlayRe.Structure;
using UnityEngine;

namespace PlayRe.Records
{
    [System.Serializable]
    public class TransformRecord : Record
    {
        public string recordableId;
        
        public Position position;
        
        public Rotation rotation;

        public bool recordingPosition;

        public bool recordingQuaternion;
        
        public override void OnReplay()
        {
            var rObj = Recorder.Instance.GetRecordableObject(recordableId).transform;
            if (rObj != null)
            {
                if(recordingPosition)
                    rObj.position = position;
                
                if(recordingQuaternion)
                    rObj.rotation = rotation;
            }
        }


    }
}