using PlayRe.Internal;
using PlayRe.Records;
using UnityEngine;

namespace PlayRe.Recordables
{
   
    /// <summary>
    /// Responsible for recording positions and rotations.
    /// </summary>
    [System.Serializable]
    public class TransformRecordable : Recordable
    {
        public bool recordPosition = true;
        public bool recordQuaternion = true;
        
        /// <summary>
        /// Recordable ID of this object
        /// </summary>
        public string RecordableId
        {
            get { return RecordableIdComponent.id; }   
        }
    
        public override Record OnRecord(float time)
        {
            // Create new Record
            var r = new TransformRecord();
            var rObj = Recorder.Instance.GetRecordableObject(RecordableId).transform;
            r.recordableId = RecordableId;

            // Record data
            r.recordingPosition = recordPosition;
            r.recordingQuaternion = recordQuaternion;
            
            if(recordPosition)
                r.position = rObj.position;
            
            if(recordQuaternion)
                r.rotation = rObj.rotation;
            
            r.demoTime = time;
            
            // Finish recording
            return r;
        }

        
    }
}