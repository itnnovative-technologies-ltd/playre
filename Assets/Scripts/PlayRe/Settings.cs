﻿namespace PlayRe
{
    /// <summary>
    /// Contains PlayRe settings
    /// </summary>
    public static class Settings
    {

        /// <summary>
        /// TickRate of recording, default = 60 HZ
        /// </summary>
        public static int TickRate = 60;
    
    }
}
