using System.Collections.Generic;
using System.Linq;
using PlayRe.Internal;
using UnityEngine;

namespace PlayRe
{
    /// <summary>
    /// Recorder is just... recorder, it records things.
    /// </summary>
    public class Recorder : MonoBehaviour
    {
        private static Recorder _instance;

        public static Recorder Instance
        {
            get
            {
                if (_instance == null)
                    _instance = FindObjectOfType<Recorder>();

                if (_instance == null)
                    _instance = (new GameObject("Recorder")).AddComponent<Recorder>();

                return _instance;
            }
        }

        /// <summary>
        /// Contains list of all recordable prefabs - objects you want to record
        /// </summary>
        public List<RecordableId> recordables = new List<RecordableId>();
        
        
        /// <summary>
        /// It's a list of all "virtual recorders" - things that are used to translate specified elements and record them
        /// </summary>
        public List<Recordable> recordering = new List<Recordable>();
        
        /// <summary>
        /// Last frame time, used to determine when to record next frame
        /// </summary>
        private float _lastFrame;
        
        /// <summary>
        /// Here is the last demo recorded
        /// </summary>
        public Demo lastDemo;

        /// <summary>
        /// True if recording
        /// </summary>
        public bool recording;

        
        public void Awake()
        {
            _instance = this;
        }

        /// <summary>
        /// Starts new recording, beware to save old one :)
        /// </summary>
        [ContextMenu("New recording")]
        public void NewRecording()
        {
            lastDemo = new Demo();
            recording = true;
        }

        /// <summary>
        /// Stops the recording
        /// </summary>
        [ContextMenu("Stop recording")]
        public void StopRecording()
        {
            recording = false;
        }

        /// <summary>
        /// Registers Recordable for Recording
        /// Aka makes this thing save your data :)
        /// </summary>
        /// <param name="recordable"></param>
        public void RegisterForRecording(Recordable recordable)
        {
            if(!recordering.Contains(recordable))
                recordering.Add(recordable);
        }
        
        /// <summary>
        /// Searches DB for recordable
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public RecordableId GetRecordableObject(string id)
        {
            return recordables.FirstOrDefault(r => r.id == id);
        }

        public void Update()
        {
            if (recording)
            {
                // Offset time
                lastDemo.demoTime += Time.deltaTime;
                
                // Check if tick has passed
                if (_lastFrame + (1f / Settings.TickRate) < lastDemo.demoTime)
                {
                    // Record everything
                    foreach (var v in recordering)
                        lastDemo.records.Add(v.OnRecord(lastDemo.demoTime));
                    
                    // Register current tick
                    _lastFrame = lastDemo.demoTime;
                }
            }
        }
    }
}