﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Threading.Tasks;
using PlayRe.Internal;

namespace PlayRe
{
    /// <summary>
    /// Demo is just a replay you want to record :)
    /// </summary>
    [Serializable]
    public class Demo
    {

        /// <summary>
        /// This represents the total Demo Time (length of Demo in gameTime seconds)
        /// </summary>
        public float demoTime = 0f;
        
        /// <summary>
        /// This represents list of all "recordables" - aka all saved data
        /// </summary>
        public List<Record> records = new List<Record>();

        public byte[] Serialize()
        {
            var ms = new MemoryStream();
            var bf = new BinaryFormatter();
            bf.Serialize(ms, this);
            var data = ms.GetBuffer();
            ms.Close();

            return data;
            //return MessagePack.LZ4MessagePackSerializer.Serialize(this);
        }

        public static Demo Deserialize(byte[] data)
        {
            var ms = new MemoryStream(data);
            var bf = new BinaryFormatter();
            var demo = bf.Deserialize(ms) as Demo;
            ms.Close();
            return demo;
        }
    }
}
