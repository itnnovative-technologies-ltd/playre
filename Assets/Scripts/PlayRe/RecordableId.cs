using System;
using UnityEngine;

namespace PlayRe
{
    /// <summary>
    /// This script is important for Recordables. Allows to define which object will be modified while replay is being played.
    /// </summary>
    public class RecordableId : MonoBehaviour
    {

        /// <summary>
        /// ID of your recordable, used in other scripts to create "virtual recorders" aka... Recordables :D
        /// </summary>
        public string id = Guid.NewGuid().ToString();

        public void Awake()
        {
            Recorder.Instance.recordables.Add(this);
        }

        private void OnDestroy()
        {
            Recorder.Instance.recordables.Remove(this);
        }
    }
}