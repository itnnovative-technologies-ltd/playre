using System;
using UnityEngine;
namespace PlayRe.Structure
{
    /// <summary>
    /// Represents serializable version of Unity3D's Quaternion
    /// </summary>
    [Serializable]
    public class Rotation
    {
        public float x;
        public float y;
        public float z;
        public float w;

        public static implicit operator Quaternion(Rotation pos)
        {
            return new Quaternion(pos.x, pos.y, pos.z, pos.w);
        }
        
        public static implicit operator Rotation(Quaternion pos)
        {
            var p = new Rotation();
            p.x = pos.x;
            p.y = pos.y;
            p.z = pos.z;
            p.w = pos.w;
            return p;
        }
    }
}