using System;
using UnityEngine;

namespace PlayRe.Structure
{
    /// <summary>
    /// Represents serializable version of Unity3D's Vector3
    /// </summary>
    [Serializable]
    public class Position
    {
        public float x;
        public float y;
        public float z;

        public static implicit operator Vector3(Position pos)
        {
            return new Vector3(pos.x, pos.y, pos.z);
        }
        
        public static implicit operator Position(Vector3 pos)
        {
            var p = new Position();
            p.x = pos.x;
            p.y = pos.y;
            p.z = pos.z;
            return p;
        }
    }
}