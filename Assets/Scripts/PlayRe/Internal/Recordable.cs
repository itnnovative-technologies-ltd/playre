using UnityEngine;

namespace PlayRe.Internal
{
    /// <summary>
    /// Recordable is an automated recording component.
    /// It is inherited when you're creating custom Recording systems that stores data into Records and Demo.
    /// </summary>
    [RequireComponent(typeof(RecordableId))]
    public abstract class Recordable : MonoBehaviour
    {
        /// <summary>
        /// Automatically register this Recordable into Recorder
        /// </summary>
        public bool autoRegister = true;
        
        /// <summary>
        /// Recordable ID on this GameObject
        /// </summary>
        internal RecordableId RecordableIdComponent;
        
        /// <summary>
        /// Executed on recording frame
        /// </summary>
        /// <returns></returns>
        public abstract Record OnRecord(float time);

        /// <summary>
        /// Setup basic parameters, should not be overriden
        /// </summary>
        public void Awake()
        {
            RecordableIdComponent = GetComponent<RecordableId>();
            if (autoRegister)
                Recorder.Instance.RegisterForRecording(this);
        }
    }
}