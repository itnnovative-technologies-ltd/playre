using System;
using System.Runtime.Serialization;
using PlayRe.Recordables;
using PlayRe.Records;

namespace PlayRe.Internal
{
    /// <summary>
    /// Record is a class that stores recorded information. Including all "meta" data like position or rotation.
    /// You need to inherit this class in all custom Recordables. 
    /// </summary>
    [Serializable]
    public abstract class Record 
    {
        /// <summary>
        /// Represents time when the record has happened
        /// </summary>
        public float demoTime;
        
        /// <summary>
        /// Executed when it's time to play this :)
        /// </summary>
        public abstract void OnReplay();

    }
}