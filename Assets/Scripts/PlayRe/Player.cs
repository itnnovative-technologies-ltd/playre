using UnityEngine;

namespace PlayRe
{
    /// <summary>
    /// Player is responsible for playing your Demos.
    /// </summary>
    public class Player : MonoBehaviour
    {
        private static Player _instance;

        public static Player Instance
        {
            get
            {
                if (_instance == null)
                    _instance = FindObjectOfType<Player>();

                if (_instance == null)
                    _instance = (new GameObject("Player")).AddComponent<Player>();

                return _instance;
            }
        }

        /// <summary>
        /// Demo to play
        /// </summary>
        public Demo demo;
        
        
        #region PLAY

        /// <summary>
        /// Current play time
        /// </summary>
        public float playTime = 0f;
        
        /// <summary>
        /// True if playing demo
        /// </summary>
        public bool playing;
        
        /// <summary>
        /// Index of last element in demo that has been played
        /// </summary>
        public int lastElement = 0;
        
        #endregion

        /// <summary>
        /// Replays the Demo (returns to beginning)
        /// </summary>
        [ContextMenu("Replay")]
        public void Replay()
        {
            lastElement = 0;
            playTime = 0f;
            playing = true;
        }

        /// <summary>
        /// Pauses the demo
        /// </summary>
        [ContextMenu("Pause")]
        public void Pause()
        {
            playing = false;
        }
        
        /// <summary>
        /// Play the demo
        /// </summary>
        [ContextMenu("Play")]
        public void Play()
        {
            playing = true;
        }
        
        /// <summary>
        /// Stops the demo [pause and return to start]
        /// </summary>
        [ContextMenu("Stop")]
        public void Stop()
        {
            lastElement = 0;
            playTime = 0f;
            playing = false;
        }

        /// <summary>
        /// Gets current demo from recorder
        /// </summary>
        [ContextMenu("Get Demo from Recorder")]
        public void GetDemoFromRecorder()
        {
            var data = Recorder.Instance.lastDemo.Serialize();
            demo = Demo.Deserialize(data);
        }
        
        public void Update()
        {
            if (playing)
            {
                // Disable recording
                Recorder.Instance.recording = false;
                
                // Offset time
                playTime += Time.deltaTime;
                
                // If there are more records in demo
                if (demo.records.Count > lastElement)
                {
                    // Get next record
                    var elem = demo.records[lastElement];
                    
                    // Check if record should be played
                    while (elem.demoTime < playTime)
                    {
                        // If true then play and switch to next record to start at
                        elem.OnReplay();
                        lastElement++;

                        // Set next element and return to loop
                        if (lastElement < demo.records.Count)
                        {
                            elem = demo.records[lastElement];
                        }
                        else
                        {
                            // If it's end of replay then break loop
                            break;
                        }
                    }
                }
            }
        }

    }
}