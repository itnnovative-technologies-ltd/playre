# PlayRe
Unity 3D Replay Recoding System based on Components.

# Features
*  Lightweight
*  Recording of object positions change
*  Automated recording basing on components.
*  Easy creation of new "Recordables"

# HowTo
1. Install
2. Create Recorder and Player GameObject
3. Add Recorder and Player Scripts from PlayRe to your created GameObjects
4. Use Recorder to Record your Demo
5. Then load your Demo to Player (beware, demo must be serialised and deserializer with polymorphism support)
6. Use Player to preview your recorded Demo
7. Automate this!

# License
See LICENSE.MD

# Authors
Software is created by <br/>
<a href="https://itnnovative.com"><img src="https://itnnovative.com/assets/logo/black.png" width="300" height="65"/></a>